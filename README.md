# Docker GUI

X11-based Docker container allowing apps to be run in graphical mode.

---

Wayland users may need to install `Xwayland` to make it work.

```bash
sudo dnf install xorg-x11-server-Xwayland
```

## Build

Go to your distro of interest directory and

```bash
docker build . -t docker-gui
```

## Test Run

```bash
docker run -it --rm --net=host --env="DISPLAY" --volume="$HOME/.Xauthority:/home/user/.Xauthority:rw" docker-gui firefox
```
